import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';



@Injectable({
  providedIn: 'root'
})

export class PostDataService {
   type;

  constructor(private afd: AngularFireDatabase) { }

  addType(data){
    return this.afd.object('/types/'+ data.type).set(data);
  }

  getTypeList(){
    return this.afd.list('/types').valueChanges();
  }

  addQue(type, question){
    return this.afd.list('/types/'+type + '/question').push(question);
  }

}
