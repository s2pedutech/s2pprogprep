import { Component, OnInit } from '@angular/core';
import { PostDataService } from 'src/app/post-data.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';



@Component({
  selector: 'app-add-type',
  templateUrl: './add-type.component.html',
  styleUrls: ['./add-type.component.css']
})
export class AddTypeComponent implements OnInit {

  addTypeForm = new FormGroup({
    insertNew : new FormControl(''),
    typeList : new FormControl(''),
  });

  quesolution = new FormGroup({
    question : new FormControl(''),
    solution : new FormControl(''),
  })
 

  typeArr : any = [];
  selectedType;
  type;

  constructor(private postData: PostDataService, private router: Router) { 
    this.postData.type = this.addTypeForm.controls.typeList.value;
    console.log(postData.type);
  }
  onRowClick(){
   this.type = document.getElementById("dropdown");
   this.selectedType = this.type.options[this.type.selectedIndex].value;
   console.log(this.selectedType);
  } 
  

  addTypeFun(){
    // this.typeArr.push(this.addTypeForm.controls.insertNew.value);
    // console.log(this.typeArr);
    let obj : any ={};
    obj.type = this.addTypeForm.controls.insertNew.value;
    this.postData.addType(obj); // This will be replace by the form value.
    this.addTypeForm.reset();
    alert("New Topic is Added !!");
    
  }
  
  addQueSolFun(){
    console.log(this.selectedType);
    this.postData.addQue(this.selectedType, this.quesolution.value);
    this.quesolution.reset();
    alert("Response Saved !!");
  }

  ngOnInit() {
    this.postData.getTypeList().subscribe(success =>{
      this.typeArr = success;
      console.log(this.typeArr);
    });

   
  }

}
