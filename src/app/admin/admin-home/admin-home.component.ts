import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// work done : admin home with the child routing and the component loading.
@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  constructor(private router: Router) { }

  private loadTypeComponent = false;
  private loadQuestionComponent = false;
  private addTypeComponent = false;
  private addQueComponent = false;

  goToTypes(){
    // this.router.navigate(['/admin/type']); 
    this.loadQuestionComponent = false;
    this.addTypeComponent = false;
    this.addQueComponent = false;
    this.loadTypeComponent = true;  
  }
  goToQuestion(){
    // this.router.navigate(['/admin/question']);
    this.loadTypeComponent = false;
    this.addTypeComponent = false;
    this.addQueComponent = false;
    this.loadQuestionComponent = true;
  }

  addType(){
    // this.router.navigate(['/admin/addType']);
    this.loadQuestionComponent = false;
    this.addTypeComponent = true;
    this.addQueComponent = false;
    this.loadTypeComponent = false;
  }

  addQuestion(){
    // this.router.navigate(['/admin/addQue']);
    this.loadQuestionComponent = false;
    this.addTypeComponent = false;
    this.addQueComponent = true;
    this.loadTypeComponent = false;
  }

  ngOnInit() {
  }

}
