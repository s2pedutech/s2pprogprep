import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsComponent } from './questions/questions.component';
import { TypeComponent } from './type/type.component';
import { AddQueComponent } from './add-que/add-que.component';
import { AddTypeComponent } from './add-type/add-type.component';
import { RouterModule, Routes } from '@angular/router';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: 'adminHome',
    component: AdminHomeComponent
  },
  {
    path: 'addQue',
    component: AddQueComponent
  },
  {
    path: 'addType',
    component: AddTypeComponent
  },
  
  {
    path: 'question',
    component: QuestionsComponent
  },
  {
    path: 'type',
    component: TypeComponent
  }

];

@NgModule({
  declarations: [AdminHomeComponent ,QuestionsComponent, TypeComponent, AddQueComponent, AddTypeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule, FormsModule
  ]
})
export class AdminModule { }
