import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AngularFireModule} from '@angular/fire';
import { AngularFireDatabaseModule} from '@angular/fire/database';

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';

import { HomeComponent } from './home/home.component';
import { AngularFireAuthModule } from "@angular/fire/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCHLOKGdWOcaeT-emFiw64QhBsSlzYti3g",
  authDomain: "s2pproject-4abdc.firebaseapp.com",
  databaseURL: "https://s2pproject-4abdc.firebaseio.com",
  projectId: "s2pproject-4abdc",
  storageBucket: "s2pproject-4abdc.appspot.com",
  messagingSenderId: "1066651749783",
  appId: "1:1066651749783:web:bbdcefb78e6dcccb"
};

const routes : Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignUpComponent
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule' 
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  }
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ForgotPasswordComponent,
    LoginComponent,
    SignUpComponent,
    



  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    RouterModule.forRoot(routes),
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
