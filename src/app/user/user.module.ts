import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ListComponent } from './list/list.component';
import { QuestionsComponent } from './questions/questions.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { RouterModule, Routes } from '@angular/router';
import { UserHomeComponent } from './user-home/user-home.component';

const routes: Routes = [
  {
    path: 'userHome',
    component: UserHomeComponent
  },
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'questions',
    component: QuestionsComponent
  },
  {
    path: 'thankyou',
    component: ThankYouComponent
  }
];

@NgModule({
  declarations: [UserHomeComponent, ProfileComponent, ListComponent, QuestionsComponent, ThankYouComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class UserModule { }
